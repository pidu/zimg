'use strict'

/* global m:false */

import App from './app.js'

m.mount(document.querySelector('#app'), new App())
