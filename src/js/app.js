/* global m:false FileReader:false */

export default class App {
  constructor () {
    this.images = []
    this.zoomFactor = 1.0
    this.panLeft = 0
    this.panTop = 0
    this.dragIndicator = ''
    document.addEventListener('keydown', e => this._handleKey(e))
  }

  view () {
    if (this.images.length > 0) {
      return m('.c-image-inspector',
               {
                 style: [
                   `--zoom-factor: ${this.zoomFactor}`,
                   `--pan-top: ${this.panTop}`,
                   `--pan-left: ${this.panLeft}`
                 ].join(';')
               },
               this.images.map(url => m('.c-image', m('img', { src: url })))
              )
    } else {
      return m('.c-drop-area', {
        class: this.dragIndicator,
        ondragenter: e => this._onDragOver(e),
        ondragleave: e => this.onDragLeave(e),
        ondragover: e => this._onDragOver(e),
        ondrop: e => this._handleDrop(e)
      })
    }
  }

  _onDragLeave (e) {
    this.dragIndicator = ''
    this._noop(e)
    m.redraw()
  }

  _onDragOver (e) {
    this.dragIndicator = 'c-drop-area--active'
    this._noop(e)
    m.redraw()
  }

  _noop (e) {
    e.preventDefault()
    e.stopPropagation()
  }

  _zoomIn () {
    this.zoomFactor += 0.1
  }

  _zoomOut () {
    this.zoomFactor -= 0.1
  }

  _panLeft () {
    this.panLeft -= 5
  }

  _panRight () {
    this.panLeft += 5
  }

  _panUp () {
    this.panTop -= 5
  }

  _panDown () {
    this.panTop += 5
  }

  _fit () {
    this.panTop = 0
    this.panLeft = 0
    this.zoomFactor = 1.0
  }

  _handleKey (e) {
    switch (e.key) {
      case '+':
        this._zoomIn()
        break
      case '-':
        this._zoomOut()
        break
      case 'a':
      case 'j':
      case 'ArrowLeft':
        this._panLeft()
        break
      case 'd':
      case 'l':
      case 'ArrowRight':
        this._panRight()
        break
      case 'w':
      case 'i':
      case 'ArrowUp':
        this._panUp()
        break
      case 's':
      case 'k':
      case 'ArrowDown':
        this._panDown()
        break
      case 'Escape':
        this._fit()
    }

    this._noop(e)
    m.redraw()
  }

  _handleDrop (e) {
    const dt = e.dataTransfer
    const files = dt.files
    this._handleFiles(files)
    this._noop(e)
  }

  async _handleFiles (files) {
    const images = await Promise.all([...files].map(file => this._toDataURL(file)))
    this.images = images
    m.redraw()
  }

  _toDataURL (file) {
    return new Promise(function (resolve, reject) {
      const reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onloadend = () => {
        resolve(reader.result)
      }
    })
  }
}
