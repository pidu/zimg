OS := $(shell uname -s | tr A-Z a-z)

ifeq ($(OS),darwin)
	SHELL := /usr/local/bin/bash -O globstar
else
	SHELL := /bin/bash -O globstar
endif

build: mkdirs css js img manifest index sw compress

mkdirs:
	@mkdir -p ./public/css
	@mkdir -p ./public/img
	@mkdir -p ./public/vendor/js

css:
	@cat ./src/css/base.css > ./public/css/style.css
	@cat ./src/css/components/*.css >> ./public/css/style.css
	@cat ./src/css/tools/*.css >> ./public/css/style.css

js:
	@cp -R ./src/js ./public/
	@cp ./vendor/*.js ./public/vendor/js

img:
	@cp ./src/img/* ./public/img

manifest:
	@cp ./src/manifest.webmanifest ./public

index:
	@cp ./src/index.html ./public/index.html

sw:
	@cp ./src/service_worker.js ./public/service_worker.js

compress:
	@gzip -f -k ./public/**/*.js
	@gzip -f -k ./public/**/*.css
	@gzip -f -k ./public/**/*.html

clean:
	@rm -rf ./public
